//
//  ObavjestenjaCell.swift
//  ozon
//
//  Created by Amplitudo on 09/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class ObavjestenjaCell: UITableViewCell {

    @IBOutlet weak var obavjestenjaImage: UIImageView!
    @IBOutlet weak var obavjestenjaDate: UILabel!
    @IBOutlet weak var obavjestenjaTitle: UILabel!
    
    func updateViews(obavjestenja: Obavjestenja) {
        obavjestenjaImage.image = UIImage(named: obavjestenja.imageName)
        obavjestenjaTitle.text = obavjestenja.title
        obavjestenjaDate.text = obavjestenja.date
    }
}
