//
//  VijestiCell.swift
//  ozon
//
//  Created by Amplitudo on 08/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class VijestiCell: UITableViewCell {

    @IBOutlet weak var vijestiImage: UIImageView!
    @IBOutlet weak var vijestiDate: UILabel!
    @IBOutlet weak var vijestiTitle: UILabel! 
    
    func updateViews(vijesti: Vijesti) {
        vijestiImage.image = UIImage(named: vijesti.imageName)
        vijestiTitle.text = vijesti.title
        vijestiDate.text = vijesti.date
    }
}
