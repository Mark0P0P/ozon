//
//  ViewController2.swift
//  ozon
//
//  Created by Amplitudo on 31/12/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class SaopstenjaVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    

    @IBOutlet weak var obavjestenjaTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(rightSwipe)
        
        obavjestenjaTable.dataSource = self
        obavjestenjaTable.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getObavjestenja().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ObavjestenjaCell") as? ObavjestenjaCell{
            let obavjestenja = DataService.instance.getObavjestenja()[indexPath.row]
            cell.updateViews(obavjestenja: obavjestenja)
            return cell
        }   else {
            return ObavjestenjaCell()
        }
    }
    

}

