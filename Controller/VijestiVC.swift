//
//  ViewController.swift
//  ozon
//
//  Created by Amplitudo on 30/12/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class VijestiVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
   
    @IBOutlet weak var vijestiTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(leftSwipe)
        
        vijestiTable.dataSource = self
        vijestiTable.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getVijesti().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VijestiCell") as? VijestiCell{
            let vijest = DataService.instance.getVijesti()[indexPath.row]
            cell.updateViews(vijesti: vijest)
            return cell
        }   else {
            return VijestiCell()
        }
    }
    
}
extension UIViewController{
    @objc func swipeAction(swipe:UISwipeGestureRecognizer){
        switch swipe.direction.rawValue {
        case 1:
            performSegue(withIdentifier: "swipeRight", sender: self)
        case 2:
            performSegue(withIdentifier: "swipeLeft", sender: self)
        default:
            break 
        }
    }
}
