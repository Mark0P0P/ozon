//
//  DataService.swift
//  ozon
//
//  Created by Amplitudo on 08/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation
class DataService {
    static let instance = DataService()
    
    private let vijesti = [
        Vijesti(title: "Projekat ,,Otpad i Budućnost&#8220; pobijedio na VII Konkursu Pivare ,,Trebjesa&#8220;", imageName: "img.jpg", date: "27.12.2018"),
        Vijesti(title: "Projekat ,,Otpad i Budućnost&#8220; pobijedio na VII Konkursu Pivare ,,Trebjesa&#8220;", imageName: "img.jpg", date: "27.12.2018"),
        Vijesti(title: "Projekat ,,Otpad i Budućnost&#8220; pobijedio na VII Konkursu Pivare ,,Trebjesa&#8220;", imageName: "img.jpg", date: "27.12.2018"),
        Vijesti(title: "Projekat ,,Otpad i Budućnost&#8220; pobijedio na VII Konkursu Pivare ,,Trebjesa&#8220;", imageName: "img.jpg", date: "27.12.2018"),
        Vijesti(title: "Projekat ,,Otpad i Budućnost&#8220; pobijedio na VII Konkursu Pivare ,,Trebjesa&#8220;", imageName: "img.jpg", date: "27.12.2018")
    ]
    
    private let obavjestenja = [
        Obavjestenja(title: "Opština Pljevlja i MORiT odgovorni za nastavak  ekološke agonije u Pljevljima", imageName: "sos.jpg", date: "16.12.2018"),
        Obavjestenja(title: "Opština Pljevlja i MORiT odgovorni za nastavak  ekološke agonije u Pljevljima", imageName: "sos.jpg", date: "16.12.2018"),
        Obavjestenja(title: "Opština Pljevlja i MORiT odgovorni za nastavak  ekološke agonije u Pljevljima", imageName: "sos.jpg", date: "16.12.2018"),
        Obavjestenja(title: "Opština Pljevlja i MORiT odgovorni za nastavak  ekološke agonije u Pljevljima", imageName: "sos.jpg", date: "16.12.2018"),
        Obavjestenja(title: "Opština Pljevlja i MORiT odgovorni za nastavak  ekološke agonije u Pljevljima", imageName: "sos.jpg", date: "16.12.2018")
    ]
    
    func getVijesti() -> [Vijesti] {
        return vijesti
    }
    
    func getObavjestenja() -> [Obavjestenja]{
        return obavjestenja
    }
}
